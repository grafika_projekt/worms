//////////////////////biblioteki/////////////////////
#ifndef INIT_H
#define INIT_H

#define GLM_FORCE_RADIANS


const float PI = 3.141592653589793f;

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <math.h>
#include "include/shaderprogram.h"
#include "include/objloader.hpp"
#include "include/texture.hpp"



using namespace glm;


#endif
